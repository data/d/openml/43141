# OpenML dataset: ACSIncome

https://www.openml.org/d/43141

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The ACSIncome dataset is one of five datasets created by Ding et al. as an improved alternative to the popular UCI Adult dataset. The authors compiled data from the American Community Survey (ACS) Public Use Microdata Sample (PUMS). Data is provided for all 50 states and Puerto Rico. This upload represents the raw data from only 2018. The data contains 1,664,500 rows, 10 features, and 1 target variable. An additional column for the state code is provided for convenience.  All columns are described in the original publication (https://arxiv.org/pdf/2108.04884.pdf) as well as in the PUMS Data Dictionary (
https://www2.census.gov/programs-surveys/acs/tech_docs/pums/data_dict/PUMS_Data_Dictionary_2018.pdf). Additional detail can also be found on the author's GitHub: https://github.com/zykls/folktables/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43141) of an [OpenML dataset](https://www.openml.org/d/43141). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43141/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43141/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43141/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

